{
    "env": {
        "node": true,
        "browser": true,
        "es6": true
    },
    "plugins": ["import", "react", "flowtype", "jsx-a11y"],
    "extends": [
        "eslint:recommended",
        "plugin:import/errors",
        "plugin:import/warnings",
        "plugin:react/recommended",
        "plugin:flowtype/recommended",
        "plugin:jsx-a11y/recommended"
    ],
    "parser": "babel-eslint",
    "parserOptions": {
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true,
            "jsx": true
        },
        "sourceType": "module"
    },
    "globals": {
        "_": true,
        "moment": true
    },
    "rules": {
        "indent": ["error", 4, { "SwitchCase": 1 }],
        "comma-dangle": ["warn", "never"],
        "linebreak-style": ["off", "unix"],
        "no-trailing-spaces": ["warn"],
        "brace-style": ["error", "1tbs", { "allowSingleLine": true }],
        "quotes": ["warn", "single"],
        "semi": ["error", "always"],
        "arrow-parens": ["error", "always"],
        "no-console": ["off"],
        "no-unused-vars": ["warn"],
        "guard-for-in": ["warn"],
        "valid-typeof": ["error"],
        "spaced-comment": ["error", "always"],
        "array-bracket-newline": ["error", "consistent"],
        "object-curly-newline": ["error", { "consistent": true }],
        "newline-per-chained-call": ["error", { "ignoreChainWithDepth": 2 }],
        "react/prop-types": [
            "warn",
            {
                "ignore": ["children", "className", "dispatch", "match", "state"],
                "skipUndeclared": true
            }
        ],
        "jsx-a11y/label-has-for": [
            "warn",
            {
                "components": [],
                "required": {
                    "some": ["nesting", "id"]
                },
                "allowChildren": true
            }
        ],
        "jsx-a11y/no-autofocus": ["off"]
    }
}
