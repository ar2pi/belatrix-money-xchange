# Belatrix money-xchange

See it in action -> http://belatrix-money-xchange.herokuapp.com/

### Install / start / build

```yarn install```  
```yarn run start```  
```yarn run build```  

### Uses

1. Fixer API (required)
2. react 16
3. redux & redux-thunk
4. axios
5. semantic-ui-react
6. scss

Why ?

1. Get currency conversion rates
2. Because, hype
3. Let components communicate between each other (loading state, xhr response, etc.), also enabling thunk super powers to dispatch actions from actions :astonished: and set teh 10min expire timeout for eg. 
4. Custom client interface
5. Default styling looks good and had in mind how to use to get desired output (grids, form components, etc.)
6. Some custom styling was needed

### Features

- Custom API client to fetch Fixer's data
- Responsive UI
- Currency amount formatting
- PWA w/ service worker, local storage, web manifest and all the goot stuff

### Notes

Provided API endpoint seemed out of date (http://api.fixer.io/latest?base=USD&symbols=EUR), basically just replaced it with http://data.fixer.io/api/latest?access_key=ACCESS_TOKEN&symbols=USD&format=1  
Limitations: now seems to have subscriptions plans, free one not including setting base currency, only accepts EUR as default... which is foine for project needs anywayyyys  
Also unable to comunicate via https...
