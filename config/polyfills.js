'use strict';

// In tests, polyfill requestAnimationFrame since jsdom doesn't provide it yet.
// We don't polyfill it in the browser--this is user's responsibility.
if (process.env.NODE_ENV === 'test') {
    require('raf').polyfill(global);
}

// yea, let's just provide fetch, wessa no like, but wessa are comprehensive people
require('whatwg-fetch');

require('core-js/fn/object/assign');
// require('core-js/fn/object/keys'); // even your grandma supports Object.keys()...
require('core-js/fn/object/values');
require('core-js/fn/object/entries');
// require('core-js/fn/object/freeze');
// require('core-js/fn/object/seal');
// require('core-js/fn/object/is-frozen');
// require('core-js/fn/object/is-sealed');
// require('core-js/fn/symbol');
require('core-js/fn/promise');
// require('core-js/fn/map');
// require('core-js/fn/weak-map');
// require('core-js/fn/set');
// require('core-js/fn/weak-set');
require('core-js/fn/string/repeat'); // jeje who needs lodash when there's es6 right ? :3
require('core-js/fn/number/is-nan');