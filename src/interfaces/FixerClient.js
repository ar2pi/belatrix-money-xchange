import axios from 'axios';

/**
 * @var {string} HOSTNAME
 */
const HOSTNAME = 'data.fixer.io';
/**
 * @var {number} PORT
 */
// const PORT = 443;
const PORT = 80;
/**
 * @var {string} API_PREFIX
 */
const API_PREFIX = 'api';
/**
 * @var {string} ACCESS_KEY
 */
const ACCESS_KEY = '813e77f99e5042d247b0c947d20e3f78';

/**
 * @description
    import Client from '../../interfaces/Client';

    const client = new Client([access_key [, api_prefix]]);
    client
        .load(endpoint, payload)
        .then((data) => {
            // ...
        })
        .catch((e) => {
            // ...
        });

    OR

    class CustomService extends Client {
        someMethod(stuff) {
            console.log(this.getHost());
            // ...
        }
    }
 */
class Client {
    constructor(accessKey = ACCESS_KEY, prefix = API_PREFIX) {
        this.accessKey = accessKey;
        this.prefix = prefix;
    }

    getHostName() {
        return HOSTNAME;
    }
    getPort() {
        return PORT;
    }
    getPrefix() {
        return this.prefix;
    }
    getHost() {
        return `http://${this.getHostName()}:${this.getPort()}/${
            this.getPrefix() ? this.getPrefix() + '/' : ''
        }`;
    }

    load(endpoint = '', payload = {}) {
        return new Promise((resolve, reject) => {
            axios
                .get(`${this.getHost()}${endpoint}`, {
                    params: {
                        access_key: this.accessKey,
                        ...payload
                    }
                })
                .then(
                    (response) => {
                        if (response.data.error) {
                            reject(response.data.error);
                        }
                        resolve(response.data);
                    },
                    (reason) => {
                        reject(reason);
                    }
                );
        });
    }
}

export default Client;
