import { combineReducers } from 'redux';

export function error(state = null, action) {
    switch (action.type) {
        case 'XCHANGE_ERROR':
            return action.error;

        case 'XCHANGE_RESET':
            return null;

        default:
            return state;
    }
}

export function loading(state = false, action) {
    switch (action.type) {
        case 'XCHANGE_LOADING':
            return action.loading;

        case 'XCHANGE_RESET':
            return false;

        default:
            return state;
    }
}

export function data(state = {}, action) {
    switch (action.type) {
        case 'XCHANGE_SUCCESS':
            return action.data;

        case 'XCHANGE_RESET':
            return {};

        default:
            return state;
    }
}

export default combineReducers({ error, loading, data });
