const currency = (value, symbol = '') => {
    // first, we make sure provided value is ok to format
    let amount = number(value);
    if (amount === null) {
        console.log('hehehehe', value, symbol, amount);
        return '0.00';
    }
    // and here be the wackamole
    // note: parsing floats as string might lead to some unexpected results with 16+ decimals
    // hence the handy arbitrary 4 decimals limit here...
    // a more precise approach could be undertaken if need be
    return amount.toFixed(4).replace(/^([^.]*)(\..*)$/, (m, p1, p2) => {
        // remove trailing zeroes, preserving at least 2 decimals precision
        let d = p2.replace(/^(\.\d{2}(?:\d*[1-9])*)(?:0+)?$/, '$1');
        return (
            symbol +
            ' ' +
            p1.replace(/./g, (c, i, a) => {
                return i > 0 && (a.length - i) % 3 === 0 ? ',' + c : c;
            }) +
            d
        );
    });
};

const number = (value) => {
    let n;
    if (typeof value === 'number') {
        n = value;
    } else if (typeof value === 'string') {
        n = +value.replace(/[^\d.]/gm, '');
    }
    if (Number.isNaN(n)) {
        return null;
    }
    return n;
};

export default { currency, number };
