import FixerClient from '../interfaces/FixerClient';

const client = new FixerClient();

export function error(error) {
    return { type: 'XCHANGE_ERROR', error };
}

export function loading(loading) {
    return { type: 'XCHANGE_LOADING', loading };
}

export function success(data) {
    return { type: 'XCHANGE_SUCCESS', data };
}

export function load(endpoint, queryParams = {}) {
    return (dispatch) => {
        dispatch(loading(true));
        dispatch(error(''));

        return client
            .load(endpoint, queryParams)
            .then((data) => {
                dispatch(loading(false));
                dispatch(success(data));
                let rates = Object.keys(data.rates);
                rates.forEach((symbol) => {
                    localStorage.setItem(`xchangeRate_${symbol}`, data.rates[symbol]);
                });
                setTimeout(() => {
                    dispatch(reset(rates));
                }, 10 * 60 * 1000); // 10 minutes expiry
            })
            .catch((e) => {
                dispatch(loading(false));
                dispatch(error(e));
            });
    };
}

export function reset(rates) {
    rates.forEach((symbol) => {
        localStorage.removeItem(`xchangeRate_${symbol}`);
    });
    return { type: 'XCHANGE_RESET' };
}
