import React from 'react';
import { Segment } from 'semantic-ui-react';
import XChangeForm from '../components/XChangeForm';

const HomePage = () => (
    <Segment>
        <XChangeForm
            currencyFrom={{ name: 'USD', label: 'US Dollar', symbol: '$' }}
            currencyTo={{ name: 'EUR', label: 'Euro', symbol: '€' }}
        />
    </Segment>
);

export default HomePage;
