import React from 'react';

const NotFoundPage = () => (
    <div className="not-found">
        <h1 className="text-center">Not found</h1>
    </div>
);

export default NotFoundPage;
