import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import '../assets/css/semantic.css';
import '../assets/css/main.css';

class App extends Component {
    render() {
        return (
            <div id="app" className="main__wrapper">
                <Header />
                <main className="main">
                    <Container>
                        {this.props.children}
                    </Container>
                </main>
                <Footer />
            </div>
        );
    }
}

export default App;
