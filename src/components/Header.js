import React from 'react';
import { Image } from 'semantic-ui-react';

const Header = () => {
    return (
        <header className="header">
            <Image src="/img/icon/exchange.svg" size="small" centered />
        </header>
    );
};

export default Header;
