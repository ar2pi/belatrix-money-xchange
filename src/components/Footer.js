import React from 'react';
import { Container, Grid, Image } from 'semantic-ui-react';

const Footer = () => {
    return (
        <footer className="footer">
            <Container>
                <Grid centered padded>
                    <Grid.Column width={4} className="hidden-palm">
                        <Image src="/img/wireframe/paragraph.png" size="small" centered />
                    </Grid.Column>
                    <Grid.Column width={4} className="hidden-palm">
                        <Image src="/img/wireframe/paragraph.png" size="small" centered />
                    </Grid.Column>
                    <Grid.Column width={4} className="hidden-palm">
                        <Image src="/img/wireframe/paragraph.png" size="small" centered />
                    </Grid.Column>
                    <Grid.Column width={4}>
                        <Image src="/img/wireframe/media-paragraph.png" size="small" centered />
                    </Grid.Column>
                </Grid>
            </Container>
        </footer>
    );
};

export default Footer;
