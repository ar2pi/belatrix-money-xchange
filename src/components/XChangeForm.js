import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Form, Icon, Message } from 'semantic-ui-react';
import { load as xchangeLoad } from '../actions/xchange';
import format from '../helpers/format';
import validator from 'validator';

const convertCurrencyValues = (rate, { fromValue, fromSymbol }, toSymbol) => {
    return {
        from: format.currency(fromValue, fromSymbol),
        to: format.currency((format.number(fromValue) / format.number(rate)).toFixed(4), toSymbol)
    };
};

class XChangeForm extends Component {
    static propTypes = {
        currencyFrom: PropTypes.object.isRequired,
        currencyTo: PropTypes.object.isRequired
    };

    state = {
        loading: false,
        errors: {},
        values: {
            amountFrom: '',
            amountTo: ''
        }
    };

    constructor(props) {
        super(props);
        this.state.currencyFrom = props.currencyFrom;
        this.state.currencyTo = props.currencyTo;
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        let derivedState = {};
        if (nextProps.error && !prevState.errors.xchange) {
            derivedState.errors = {
                xchange: 'Error while fetching conversion rates, please try again later'
            };
        }
        if (nextProps.data.success && validator.isEmpty(prevState.values.amountTo)) {
            let { from, to } = convertCurrencyValues(
                nextProps.data.rates[nextProps.currencyFrom.name],
                {
                    fromValue: prevState.values.amountFrom,
                    fromSymbol: prevState.currencyFrom.symbol
                },
                prevState.currencyTo.symbol
            );
            derivedState.values = {
                ...prevState.values,
                amountFrom: from,
                amountTo: to
            };
        }
        if (nextProps.loading !== prevState.loading) {
            derivedState.loading = nextProps.loading;
        }
        return _.isEmpty(derivedState) ? null : derivedState;
    }

    handleChange = (key) => {
        return (e, data) => {
            this.changeValue(key, data.value);
        };
    };

    changeValue = (key, value) => {
        this.setState((prevState) => ({
            errors: {},
            values: {
                ...prevState.values,
                [key]: value
            }
        }));
    };

    handleFocusCurrencyFrom = () => {
        this.changeValue('amountFrom', '');
        this.changeValue('amountTo', '');
    };

    handleSubmit = (e) => {
        e.preventDefault();
        if (validator.isEmpty(this.state.values.amountFrom)) {
            this.setState(() => ({
                errors: {
                    amountFrom: 'Please provide a value to convert to'
                }
            }));
            return;
        }
        this.setState(() => ({ errors: {} }));
        let storedRate;
        if (!_.isEmpty(this.props.data) && this.props.data.hasOwnProperty('rates')) {
            storedRate = this.props.data.rates[this.props.currencyFrom.name];
        } else {
            storedRate = localStorage.getItem(`xchangeRate_${this.state.currencyFrom.name}`);
        }
        if (storedRate === void 0 || storedRate === null) {
            this.props.dispatch(
                xchangeLoad('latest', { symbols: this.state.currencyFrom.name, format: 1 })
            );
            return;
        }
        let { from, to } = convertCurrencyValues(
            storedRate,
            { fromValue: this.state.values.amountFrom, fromSymbol: this.state.currencyFrom.symbol },
            this.state.currencyTo.symbol
        );
        this.changeValue('amountFrom', from);
        this.changeValue('amountTo', to);
    };

    render() {
        return (
            <Form className="xchange-form">
                <h3>Money-XChange</h3>
                {Object.values(this.state.errors).map((error, i) => (
                    <Message key={i} negative>
                        {error}
                    </Message>
                ))}
                <Form.Group widths="equal">
                    <Form.Input
                        type="text"
                        id="amountFrom"
                        name="amountFrom"
                        label="Enter amount in USD"
                        placeholder={`${this.state.currencyFrom.name} - ${
                            this.state.currencyFrom.label
                        }`}
                        value={this.state.values.amountFrom}
                        onChange={this.handleChange('amountFrom')}
                        onFocus={this.handleFocusCurrencyFrom}
                        className="xchange-form__input"
                        error={!!this.state.errors.amountFrom}
                        fluid
                        autoFocus
                    />
                    <Icon
                        name="exchange"
                        size="large"
                        color="grey"
                        className={'xchange-form__icon ' + this.state.rotateIcon}
                        fitted
                    />
                    <Form.Input
                        type="text"
                        id="amountTo"
                        name="amountTo"
                        placeholder={`${this.state.currencyTo.name} - ${
                            this.state.currencyTo.label
                        }`}
                        value={this.state.values.amountTo}
                        onChange={this.handleChange('amountTo')}
                        className="xchange-form__input no-focus"
                        readOnly
                        fluid
                    />
                </Form.Group>
                <Form.Group>
                    <Form.Button
                        primary
                        type="submit"
                        size="large"
                        loading={this.state.loading}
                        onClick={this.handleSubmit}
                        className="xchange-form__submit"
                        fluid
                    >
                        Calculate
                    </Form.Button>
                </Form.Group>
            </Form>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        data: state.xchange.data,
        error: state.xchange.error,
        loading: state.xchange.loading
    };
};

export default connect(mapStateToProps)(XChangeForm);
